#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <util/delay.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include "timer.h"
#include "sreg.h"
#include "char_lcd.h"
#include "btn.h"
#include "serial.h"
// numbers
#define NUM_STATES 5
#define READY_STATE 0
#define CONFIG_STATE 1
#define WORK_STATE 2
#define REST_STATE 3
#define LONG_REST_STATE 4
#define LONG_REST_FREQUENCY 4 //every 4 cycles, take a long break
#define CONFIG_OFFSET 2 //when do configurable states start
#define NUM_CONFIGURABLE 2 //how many states can we configure (0 index)

#define NUM_BTNS 3
/**
 * Struct to hold information about some machine state
 */
typedef struct PomState {
  char* msg;
  unsigned char timed;
  unsigned char timeout;
  void (*btnMethods[NUM_BTNS])();
  void (*nextState)();/**< next state in pattern **/
} PomState, *PPomState;

/**
 * Sets currentState to first state
 */
void stopTimer();

/**
 * Increment `configState`.
 * If we run out of states, set to NULL
 * If starts at NULL, sets to offset
 */
void nextConfigStep();

/**
 * Decrease duration of state currently being configured
 * uses `configState` as state to change
 */
void increaseTimeout();

/**
 * Increase duration of state currently being configured
 * uses `configState` as state to change
 */
void decreaseTimeout();

/**
 * If the timeout in state has expired, and we need to progress
 */
int stateExpired(unsigned long, unsigned long, PPomState);

/**
 * Sets currentState to WORK_STATE
 */
void startWork();
void startRest();

void redrawLcd(Pdisplay, char*, char*);
