#include "main.h"
#define time_interval SECS

/*
 * Set up all possible states
 */
PomState states[NUM_STATES] = {
  /* msg, timed, timeout, btn1(), btn2(), btn3()*/
  {"Ready",    0, 0, {&startWork,      NULL,             &nextConfigStep},   NULL},
  {"Setup",    0, 0, {&increaseTimeout, &decreaseTimeout, &nextConfigStep}, NULL},
  {"Work",     1, 2, {&stopTimer,   NULL,             NULL},            &startRest},
  {"Break",    1, 1, {&stopTimer,   NULL,             NULL},            &startWork},
  {"Recharge", 1, 3, {&stopTimer,   NULL,             NULL},            &startWork}
};
PPomState state = states; //start in ready state
PPomState configState = NULL;

Button btns[NUM_BTNS] = {
 /* pressed, debounce, timer, waiting */
  {0, 100, &timer_time, 0},
  {0, 100, &timer_time, 0},
  {0, 100, &timer_time, 0}
};
                   /* port, in, out, clk, rst */
sreg displayData = {&PORTD, 0x04 ,0x08, 0x10, 0x02};
                      /* port, data sreg, rs, rw, en */
display charDisplay = {&PORTD, &displayData, 0x20, 0x40, 0x80};
unsigned char repeats = 0;

void setup(void) {
  timer_init();
  uart_init();

  //portd pins 3,4,5,6,7 outputs
  DDRD = (0x02 | 0x04 | 0x08 | 0x10 | 0x20 | 0x40 | 0x80);

  //portb pins 0,1 inputs, with interrupts enabled
  DDRB = ~(0x01 | 0x02 | 0x04);
  PCICR |= 0x01;
  PCMSK0 |= (0x01 | 0x02 | 0x04);

  sreg_init(&displayData); //initialise shift register
  lcd_init(&charDisplay, 0x19, 0x02, 0x04); //initialise display

  sei();
}

int main(void) {
  FILE out = FDEV_SETUP_STREAM(uart_putchar, NULL, _FDEV_SETUP_WRITE);
  stdout = &out;
  setup();
  unsigned long now = timer_time(time_interval);
  unsigned long then = now;
  char* bottomText = calloc(LCD_NUM_COLS, LCD_NUM_COLS*sizeof(char));
  char* topText = calloc(LCD_NUM_COLS, LCD_NUM_COLS*sizeof(char));
  unsigned char redraw = 1;
  while(1) {
    now = timer_time(time_interval);
    for(int i = 0; i < NUM_BTNS; i++){
      if(btn_pressed(&btns[i])) {
        then = now;
        state->btnMethods[i]();
        redraw = 1;
      }
    }
    if(state->timeout && stateExpired(now, then, state)){
      then = now;
      state->nextState();
      redraw = 1;
    }
    if(redraw) {
      redrawLcd(&charDisplay, topText, bottomText, then, now);
      redraw = 0;
    }
  }
}

void redrawLcd(Pdisplay d, char* bottomText, char* topText, int then, int now) {
  lcd_write_cmd(d, LCD_CLEAR_DISP);
  sprintf(topText, "%s", state->msg);
  lcd_write_center(d, LCD_FIRST_CHAR, topText);
  if(configState) {
    sprintf(bottomText, "%s:%ds", configState->msg, configState->timeout);
    lcd_write_center(d, LCD_SECOND_CHAR, bottomText);
  } else if(state->timeout) {
    sprintf(bottomText, "%ds", state->timeout - (now - then));
    lcd_write_center(d, LCD_SECOND_CHAR, bottomText);
  }
}

void startRest() {
  if (repeats >= LONG_REST_FREQUENCY) {
    repeats = 0;
    state = &states[LONG_REST_STATE];
  } else {
    repeats++;
    state = &states[REST_STATE];
  }
}

int stateExpired(unsigned long now, unsigned long then, PPomState s) {
  return (now - then) >= s->timeout;
}

void stopTimer() {
  state = &states[READY_STATE];
}

void startWork() {
  state = &states[WORK_STATE];
}

void nextConfigStep() {
  if(configState == NULL){
    configState = &states[CONFIG_OFFSET];
    state = &states[CONFIG_STATE];
  } else if (configState < states + CONFIG_OFFSET + NUM_CONFIGURABLE) {
    configState++;
  } else {
    configState = NULL;
    stopTimer(NULL);
  }
}

void increaseTimeout() {
  if(configState) {
    configState->timeout++;
  }
}

void decreaseTimeout() {
  if(configState) {
    configState->timeout--;
  }
}

ISR(PCINT0_vect) {
  if(PINB & (1 << 0)) {
    btn_press(&btns[0]);
  }
  if(PINB & (1 << 1)) {
    btn_press(&btns[1]);
  }
  if(PINB & (1 <<2)) {
    btn_press(&btns[2]);
  }
}
