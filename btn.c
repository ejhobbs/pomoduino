#include "btn.h"
int btn_pressed(PButton b) {
  if (b->waiting && b->timer(MILLIS) - b->lastPressed > b->debounce) {
    b->waiting = 0;
    return 1;
  } else {
    return 0;
  }
}

void btn_press(PButton b) {
  b->lastPressed = b->timer(MILLIS);
  b->waiting = 1;
}
