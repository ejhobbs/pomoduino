# Pomoduino
An Open Source pomodoro style timer for the Arduino platform.

## Features
None

## Planned Features
+ Custom Patterns
+ On-The-Fly configurable periods (Work, Short Rest, Long Rest)
+ LED indication of current stage
+ Countdown timer
