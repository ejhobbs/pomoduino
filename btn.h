#ifndef LIB_BTN
#define LIB_BTN
#include "lib/timer/timer.h"

/**
 * Struct for storing the state of a button
 */
typedef struct Button {
  volatile unsigned long lastPressed;
  volatile unsigned int debounce;
  unsigned long (*timer)(period);
  volatile unsigned int waiting;
} Button, *PButton;

/**
 * If we can process the button press yet
 * @param time Current time
 * @returns If debounce period has elapsed since last press
 */
int btn_pressed(PButton);

/**
 * Mark button as having been pressed
 */
void btn_press(PButton);

#endif /*LIB_BTN*/
